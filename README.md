# Kubernetes

## What is Kubernetes?

## Why we use kubernetes?

## Features of kubernetes?

## Kubernetes Architecture?

## Pod Lifecycle

## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects
- ReplicationController - Manage replicas of the pod
- ReplicaSet - Manage replicas of the pod
- deployment - Manage replicas of the pod through ReplicaSet. Manages how pods are going to be managed
- StatefulSet - Manage replicas of the Stateful Pod
- DaemonSet - Manage pods as daemon on each node
- ConfigMap - To store variable
- Secret - To store credentials or keys